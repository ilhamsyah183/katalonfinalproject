<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Facility                               _cc113f</name>
   <tag></tag>
   <elementGuidId>cec28745-a6f8-4b60-a766-3b5a6df11c73</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.panel-body</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='history']/div/div[2]/div/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b64e1cc3-cdf3-4542-a7c3-0d979e919005</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>panel-body</value>
      <webElementGuid>b7ebbea4-2ca5-4067-bf90-e4bb929af2c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                            Facility
                        
                        
                            Hongkong CURA Healthcare Center
                        
                        

                        
                            Apply for hospital readmission
                        
                        
                            Yes
                        
                        

                        
                            Healthcare Program
                        
                        
                            Medicaid
                        
                        

                        
                            Comment
                        
                        
                            Test History Appointment
                        
                    </value>
      <webElementGuid>76961ba6-9c04-417c-9285-b974e78439d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;history&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-offset-2 col-sm-8&quot;]/div[@class=&quot;panel panel-info&quot;]/div[@class=&quot;panel-body&quot;]</value>
      <webElementGuid>6dd20947-3463-447a-8783-404dd999355d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='history']/div/div[2]/div/div/div[2]</value>
      <webElementGuid>912b9c3f-9aca-47d7-99f5-b5d0829d0e06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='History'])[2]/following::div[5]</value>
      <webElementGuid>512c2b9d-4653-4733-a576-3e90dde8cd8d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[1]/following::div[8]</value>
      <webElementGuid>f4a7bdf3-5087-44f9-a397-66fd89747e2d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div[2]</value>
      <webElementGuid>f01259e4-3509-4ff9-99ed-edc74f519a5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                            Facility
                        
                        
                            Hongkong CURA Healthcare Center
                        
                        

                        
                            Apply for hospital readmission
                        
                        
                            Yes
                        
                        

                        
                            Healthcare Program
                        
                        
                            Medicaid
                        
                        

                        
                            Comment
                        
                        
                            Test History Appointment
                        
                    ' or . = '
                        
                            Facility
                        
                        
                            Hongkong CURA Healthcare Center
                        
                        

                        
                            Apply for hospital readmission
                        
                        
                            Yes
                        
                        

                        
                            Healthcare Program
                        
                        
                            Medicaid
                        
                        

                        
                            Comment
                        
                        
                            Test History Appointment
                        
                    ')]</value>
      <webElementGuid>61fff2f4-10ec-419c-b72e-89500b6e5e3d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
