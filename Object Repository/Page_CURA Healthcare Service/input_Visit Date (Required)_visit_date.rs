<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Visit Date (Required)_visit_date</name>
   <tag></tag>
   <elementGuidId>4eb05714-c44d-4058-9748-606635e6b8fc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#txt_visit_date</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='txt_visit_date']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>11c4532d-5b5d-462f-84fc-3e029f0b637d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>cd7806ce-3a11-4679-96bb-a845636b5947</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>8dca6b73-a888-4c6b-9424-01e4aa9eceb6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>txt_visit_date</value>
      <webElementGuid>3c212c5e-4cad-4ac3-bb19-24b64ac66ffb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>visit_date</value>
      <webElementGuid>aad678d9-59b3-484a-8c84-b814af9d16f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>dd/mm/yyyy</value>
      <webElementGuid>1fe48345-686c-4330-9016-2b93fad44813</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>be31ae04-7237-4ae8-8ea3-be36ed3bd505</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;txt_visit_date&quot;)</value>
      <webElementGuid>f54e4fe1-d6e4-4027-9fe6-778a818ab4c7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='txt_visit_date']</value>
      <webElementGuid>77e8bb6e-b954-4f87-b035-20887a2efe9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[4]/div/div/input</value>
      <webElementGuid>3694919a-caab-4e91-a8be-e93a110feb8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/input</value>
      <webElementGuid>bb124a17-ee27-438a-af27-6fd7807b9f2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @id = 'txt_visit_date' and @name = 'visit_date' and @placeholder = 'dd/mm/yyyy']</value>
      <webElementGuid>b1cd6096-07aa-41ca-8b79-3785ff69a1bc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
