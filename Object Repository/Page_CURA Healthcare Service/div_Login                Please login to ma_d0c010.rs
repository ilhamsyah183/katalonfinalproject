<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Login                Please login to ma_d0c010</name>
   <tag></tag>
   <elementGuidId>05ce619d-25d4-4953-9762-db0bf7c0eea8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-sm-12.text-center</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='login']/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>6f8ef8ed-5ccf-4a03-b37d-678658712633</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-sm-12 text-center</value>
      <webElementGuid>21620fee-c545-46e7-8944-f297b1cfaf1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Login
                Please login to make appointment.
                                    Login failed! Please ensure the username and password are valid.
                            </value>
      <webElementGuid>f61fb337-cbf9-43b2-9228-c85b5c89d947</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-12 text-center&quot;]</value>
      <webElementGuid>40bf7756-5548-4ac9-ae2a-892261d77b75</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='login']/div/div/div</value>
      <webElementGuid>8c74e852-323d-468b-ba68-51a6a4145f1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[1]/following::div[3]</value>
      <webElementGuid>f5fa3af4-4024-4107-b945-40b138564c3b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='We Care About Your Health'])[1]/following::div[3]</value>
      <webElementGuid>485cf1a3-fa96-4837-90f9-28811fc4157e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Demo account'])[1]/preceding::div[1]</value>
      <webElementGuid>7123dfc8-f3ad-4ba2-96b0-479ffb6effb0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div</value>
      <webElementGuid>7aa4d3e9-ffc5-43f7-a34e-de8c1c74197b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                Login
                Please login to make appointment.
                                    Login failed! Please ensure the username and password are valid.
                            ' or . = '
                Login
                Please login to make appointment.
                                    Login failed! Please ensure the username and password are valid.
                            ')]</value>
      <webElementGuid>84d56e85-88ad-4563-a0f4-d87ef139f7c9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
